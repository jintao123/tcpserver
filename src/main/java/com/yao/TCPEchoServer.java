package com.yao;

import com.yao.util.DBHelper;
import com.yao.util.DateUtil;
import com.yao.util.HexUtils;
import com.yao.util.StringUtil;
import com.yao.util.model.ClassTableRealation;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: tao95
 * Date: 2017/4/26
 * Time: 16:58
 * To change this template use File | Settings | File Templates.
 * Description:
 */
public class TCPEchoServer {
    private static final int BUFSIZE=32;

    public static void main(String[] args) {
        statrServer();
    }
    public static void statrServer(){
        int servPort = 9999;
        ServerSocket servSocket =null;
        int recvMsgSize=0;
        byte[] receivBuf=new byte[BUFSIZE];
        try {
            StringUtil strToHex = new StringUtil();
            //启动服务端口,监听请求
            servSocket=new ServerSocket(servPort);
            System.out.println("server is starting.........");
            while(true){
                //接收请求，建立连接
                Socket clientSocket=servSocket.accept();
                SocketAddress clientAddress = clientSocket.getRemoteSocketAddress();
                System.out.println("Handling client at "+ clientAddress);
                InputStream in =clientSocket.getInputStream();
                DataInputStream input = new DataInputStream(clientSocket.getInputStream());
                OutputStream out= clientSocket.getOutputStream();
                //读取终端传递的数据，并做相应的处理
                while((recvMsgSize=input.read(receivBuf))!=-1) {
                    String temp = StringUtil.bytes2HexString(receivBuf);
                    //实验室id
                    int laboratory_id = 1;
                    //表示当前时间为星期几的变量
                    int dayOfWeek = DateUtil.getWeekOfDate(new Date());
                    //表示当前为今天的第几节课
                    int classOfDay = DateUtil.getClassOfDay();
                    if (dayOfWeek == 0 || dayOfWeek == 6) {
                        dayOfWeek = 1;
                        classOfDay = 1;
                    }
                    System.out.println("classOfDay:" + classOfDay + ":" + StringUtil.convertUTF8ToString(temp));
                    //查询当前时间对应的课程
                    ClassTableRealation ctr0 = DBHelper.getCourseTable(dayOfWeek, classOfDay, laboratory_id, 0);
                    //查询当前时间对应的下一节课程
                    ClassTableRealation ctr1 = null;
                    if(classOfDay == 4){
                        ctr1 = DBHelper.getCourseTable(dayOfWeek+1, 1, laboratory_id, 1);
                    }else{
                        ctr1 = DBHelper.getCourseTable(dayOfWeek, classOfDay + 1, laboratory_id, 1);
                    }
                    //将数据回显至物理终端
                    sendDataToLed(ctr0, ctr1, out);
                }
                clientSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static void sendDataToLed(ClassTableRealation ctr0,ClassTableRealation ctr1,OutputStream out)
            throws IOException, InterruptedException {
        String cls = "abcdefghijc";
        String className1 = "无";
        String className2 = "无";
        String courseName1 = "无";
        String courseName2 = "无";
        if(ctr0.getCourseName() != null && !"".equals(ctr0.getCourseName())){
            courseName1 = ctr0.getCourseName();
        }
        if(ctr0.getClassName() != null && !"".equals(ctr0.getClassName())){
            className1 = ctr0.getClassName();
        }
        if(ctr1.getCourseName() != null && !"".equals(ctr1.getCourseName())){
            courseName2 = ctr1.getCourseName();
        }
        if(ctr1.getClassName() != null && !"".equals(ctr1.getClassName())){
            className2 = ctr1.getClassName();
        }
        String tmp0 = "abcdefghij"+"w00"+"第一节--"+courseName1;
        String tmp1 = "abcdefghij"+"w10"+"------"+className1;
        String tmp2 = "abcdefghij"+"w20"+"第二节--"+courseName2;
        String tmp3 = "abcdefghij"+"w30"+"------"+className2;
        String hex = HexUtils.getHexResult(cls);
        hex = hex+"010000";
        byte[] receivBuf1 = StringUtil.hexStringToByteArray(hex);
        out.write(receivBuf1, 0, receivBuf1.length);
        sendDateToBuf(tmp0,out);
        sendDateToBuf(tmp1,out);
        sendDateToBuf(tmp2,out);
        sendDateToBuf(tmp3,out);
    }
    public static void sendDateToBuf(String data,OutputStream out) throws InterruptedException, IOException {
        String hex = HexUtils.getHexResult(data);
        hex = hex+"00";
        byte[] receivBuf1 = StringUtil.hexStringToByteArray(hex);
        Thread.sleep(1000);
        out.write(receivBuf1, 0, receivBuf1.length);
    }

}