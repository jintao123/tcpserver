package com.yao.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: tao95
 * Date: 2017/5/5
 * Time: 10:47
 * To change this template use File | Settings | File Templates.
 * Description:
 */
public class DateUtil {
    /**
     * 获取当前日期是星期几<br>
     *
     * @param dt
     * @return 当前日期是星期几
     */
    public static int getWeekOfDate(Date dt) {
        String[] weekDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        return w;
    }

    public static int getClassOfDay(){
        DateFormat df = new SimpleDateFormat("HH:mm:ss");//创建日期转换对象HH:mm:ss为时分秒，年月日为yyyy-MM-dd
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        String date = format.format(new Date());
        int result = 1;
        try {
            Date df_now = df.parse(date);
            Date dt1_start = df.parse("00:00:00");//将字符串转换为date类型
            Date dt1_end = df.parse("09:45:00");//将字符串转换为date类型
            Date dt2_start = df.parse("09:45:00");//将字符串转换为date类型
            Date dt2_end = df.parse("12:00:00");//将字符串转换为date类型
            Date dt3_start = df.parse("12:00:00");//将字符串转换为date类型
            Date dt3_end = df.parse("04:15:00");//将字符串转换为date类型
            Date dt4_start = df.parse("04:15:00");//将字符串转换为date类型
            Date dt4_end = df.parse("23:59:59");//将字符串转换为date类型
            if(df_now.getTime()>= dt1_start.getTime() && df_now.getTime() < dt1_end.getTime() ){
                result = 1;
            }else if(df_now.getTime()>= dt2_start.getTime() && df_now.getTime() < dt2_end.getTime()){
                result = 2;
            }else if(df_now.getTime()>= dt3_start.getTime() && df_now.getTime() < dt3_end.getTime()){
                result = 3;
            }else if(df_now.getTime()>= dt4_start.getTime() && df_now.getTime() < dt4_end.getTime()){
                result = 4;
            }

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }

}