package com.yao.util.model;

/**
 * Created with IntelliJ IDEA.
 * User: tao95
 * Date: 2017/5/5
 * Time: 11:27
 * To change this template use File | Settings | File Templates.
 * Description:
 */
public class ClassTableRealation {
    private int classOfday;
    private String className;
    private String courseName;

    public int getClassOfday() {
        return classOfday;
    }

    public void setClassOfday(int classOfday) {
        this.classOfday = classOfday;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    @Override
    public String toString() {
        return "ClassTableRealation{" +
                "classOfday=" + classOfday +
                ", className='" + className + '\'' +
                ", courseName='" + courseName + '\'' +
                '}';
    }
}